package com.example.currencyratekt

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_rate")
data class CurrencyRate(
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo("id")
  val id: Int = 0,
  @ColumnInfo("currency_id")
  val idCurrency: String = "",
  @ColumnInfo("number_code")
  val numCode: String = "",
  @ColumnInfo("char_code")
  val charCode: String = "",
  @ColumnInfo("nominal")
  val nominal: Int = 0,
  @ColumnInfo("name")
  val name: String = "",
  @ColumnInfo("current_value")
  val value: Double = 0.0,
  @ColumnInfo("previous_value")
  val previousValue: Double = 0.0
)



