package com.example.currencyratekt

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [CurrencyRate::class], version = 1)
abstract class DataBase : RoomDatabase() {
  abstract fun currencyRateDao(): CurrencyRateDao

  companion object DataBaseBuilder {
    @Volatile
    private var INSTANCE: DataBase? = null

    fun getInstance(context: Context): DataBase {
      if (INSTANCE == null) {
        synchronized(DataBase::class) {
          INSTANCE = buildRoomDb(context)
        }
      }
      return INSTANCE!!
    }

    private fun buildRoomDb(context: Context) =
      Room.databaseBuilder(
        context.applicationContext,
        DataBase::class.java, "currency_database"
      ).build()

  }

}
