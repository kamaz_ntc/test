package com.example.currencyratekt

import android.app.Application
import kotlin.concurrent.thread


class CurrencyRepository(app: Application) {

  private val db = DataBase.getInstance(app)
  private val dao = db.currencyRateDao()

  fun insertAll(currencies: Array<CurrencyRate>) {
    thread { dao.insertAll(*currencies) }
  }

}