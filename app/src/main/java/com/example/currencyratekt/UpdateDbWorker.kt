package com.example.currencyratekt

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters

class UpdateDbWorker(context: Context, params: WorkerParameters) : Worker(context, params) {
  private val contextApp = context

  override fun doWork(): Result {

    Log.d("MyTag", "Workmanager start doWork method")
    return try {
      Intent(
        applicationContext,
        UpdateDBService::class.java
      ).also { intent -> contextApp.startService(intent) }
      Result.success()
    } catch (e: Throwable) {
      e.printStackTrace()
      Result.failure()
    }
  }

}