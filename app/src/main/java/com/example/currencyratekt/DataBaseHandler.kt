package com.example.currencyratekt

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DataBaseHandler(context: Context, factory: SQLiteDatabase.CursorFactory?) :
  SQLiteOpenHelper(context, DB_NAME, factory, DB_VERSION) {
  companion object {
    private const val DB_NAME = "currency_database"
    private const val DB_VERSION = 1
    private const val TABLE_NAME = "currency_rate"
  }

  override fun onCreate(db: SQLiteDatabase?) {
    val query = "CREATE TABLE $TABLE_NAME (id INTEGER PRIMARY KEY AUTOINCREMENT, idcurrency TEXT, numcode TEXT, charcode TEXT, nominal INTEGER, name TEXT, value REAL, previous REAL)"
    db?.execSQL(query)
  }

  override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
    onCreate(db)
  }

  fun getAllCurrencyRates(): MutableMap<String, CurrencyRate> {
    val db = this.readableDatabase
    val query = "SELECT * FROM $TABLE_NAME"
    val cursor = db.rawQuery(query, null)

    val map : MutableMap<String, CurrencyRate> = LinkedHashMap()
    if (cursor.moveToFirst()) {
      do {
        map[cursor.getString(3)] = CurrencyRate(cursor.getInt(0), cursor.getString(1),
          cursor.getString(2), cursor.getString(3), cursor.getInt(4),
          cursor.getString(5), cursor.getDouble(6), cursor.getDouble(7))
      } while (cursor.moveToNext())
    }
    cursor.close()
    return map
  }
}