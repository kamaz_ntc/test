package com.example.currencyratekt

import android.app.Application
import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class UpdateDBService : Service() {

  private lateinit var currencyRepository: CurrencyRepository
  private var serviceLooper: Looper? = null
  private var serviceHandler: ServiceHandler? = null

  inner class ServiceHandler(looper: Looper) : Handler(looper) {
    override fun handleMessage(msg: Message) {
      val connection = URL(JSON_URL).openConnection() as HttpURLConnection
      val jsonString = connection.inputStream.bufferedReader().use { it.readText() }
      val jsonObject = JSONObject(jsonString)
      val map = getCurrencyMap(jsonObject.getJSONObject("Valute"))
      currencyRepository = CurrencyRepository(app = applicationContext as Application)
      currencyRepository.insertAll(map.values.toTypedArray())

      stopSelf(msg.arg1)
    }
  }

  override fun onCreate() {
    Log.d(tag, "Служба создана")
    HandlerThread("ServiceStart", Process.THREAD_PRIORITY_BACKGROUND).apply {
      start()
      serviceLooper = looper
      serviceHandler = ServiceHandler(looper)
    }
  }

  override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
    Log.d(tag, "Сервис запущен")
    serviceHandler?.obtainMessage()?.also { msg ->
      msg.arg1 = startId
      serviceHandler?.sendMessage(msg)
    }
    return START_NOT_STICKY
  }

  override fun onDestroy() {
    super.onDestroy()
    Log.d(tag, "Служба остановлена")
  }

  override fun onBind(intent: Intent): IBinder? {
    return null
  }
}