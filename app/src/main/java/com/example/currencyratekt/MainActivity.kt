package com.example.currencyratekt

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.icu.text.DecimalFormat
import android.icu.text.DecimalFormatSymbols
import android.icu.text.SimpleDateFormat
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.concurrent.thread

const val JSON_URL = "https://www.cbr-xml-daily.ru/daily_json.js"
const val tag = "MyTag"


class MainActivity : AppCompatActivity(), View.OnClickListener {
  private lateinit var currencyRepository: CurrencyRepository

  private lateinit var dataBaseHandler: DataBaseHandler
  private lateinit var mainHandler: Handler

  var currencyRateMap: MutableMap<String, CurrencyRate> = LinkedHashMap()
  var charCodeArray: MutableList<String> = LinkedList()

  var inputSpinnerPosition: Int = 0
  var outputSpinnerPosition: Int = 0

  private lateinit var arrayAdapter: ArrayAdapter<String>
  private val viewModel: CurrencyRateViewModel by viewModels { CurrencyViewModelFactory(application) }

  private lateinit var inSpinner: Spinner
  private lateinit var outSpinner: Spinner
  private lateinit var evNumber: EditText
  private lateinit var tvNumber: TextView
  private lateinit var tvInputCurrency: TextView
  private lateinit var tvOutputCurrency: TextView
  private lateinit var tvData: TextView
  private lateinit var progressBar: ProgressBar
  private lateinit var btnSave: Button
  private lateinit var btnUpdate: Button
  private lateinit var btnAdd: Button

  fun toast(message: CharSequence) =
    Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)


    inSpinner = findViewById(R.id.input_spinner)
    outSpinner = findViewById(R.id.output_spinner)
    evNumber = findViewById(R.id.in_Num)
    tvNumber = findViewById(R.id.out_Num)
    tvData = findViewById(R.id.tv_data)
    tvInputCurrency = findViewById(R.id.input_currency_text)
    tvOutputCurrency = findViewById(R.id.output_currency_text)
    progressBar = findViewById(R.id.progressBar)
    btnSave = findViewById(R.id.btn_Save)
    btnUpdate = findViewById(R.id.btn_Update)
    btnAdd = findViewById(R.id.btn_add)

    fetchData()

    inSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      @SuppressLint("SetTextI18n")
      override fun onItemSelected(
        parent: AdapterView<*>?,
        view: View?,
        position: Int,
        id: Long
      ) {
        inputSpinnerPosition = position
        currencyRateMap[charCodeArray[inputSpinnerPosition]]?.name?.let { toast(it) }
        calculateCurrencyRate(evNumber.text.toString())
        tvInputCurrency.text =
          "${currencyRateMap[charCodeArray[inputSpinnerPosition]]?.value} ${currencyRateMap[charCodeArray[inputSpinnerPosition]]?.name}"

      }

      override fun onNothingSelected(parent: AdapterView<*>?) {}
    }
    outSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      @SuppressLint("SetTextI18n")
      override fun onItemSelected(
        parent: AdapterView<*>?,
        view: View?,
        position: Int,
        id: Long
      ) {
        outputSpinnerPosition = position
        currencyRateMap[charCodeArray[outputSpinnerPosition]]?.name?.let { toast(it) }
        calculateCurrencyRate(evNumber.text.toString())
        tvOutputCurrency.text =
          "${currencyRateMap[charCodeArray[outputSpinnerPosition]]?.value} ${currencyRateMap[charCodeArray[outputSpinnerPosition]]?.name}"
      }

      override fun onNothingSelected(parent: AdapterView<*>?) {
      }
    }
    mainHandler = Handler(Looper.getMainLooper())

    evNumber.setOnClickListener(this)
    btnSave.setOnClickListener(this)
    btnUpdate.setOnClickListener(this)
    btnAdd.setOnClickListener(this)

    progressBar.visibility = View.VISIBLE
    viewModel.updateDb()
  }


  override fun onClick(v: View?) {
    when (v?.id) {
      R.id.in_Num -> calculateCurrencyRate(evNumber.text.toString())
      R.id.btn_Save -> {
        Log.d(tag, "On click insert")
        currencyRepository = CurrencyRepository(application)
        currencyRepository.insertAll(currencyRateMap.values.toTypedArray())
      }
      R.id.btn_Update -> {
        dataBaseHandler = DataBaseHandler(applicationContext, null)
        currencyRateMap = dataBaseHandler.getAllCurrencyRates()
        charCodeArray = ArrayList(currencyRateMap.keys)
        updateArrayAdapter()
      }
      R.id.btn_add -> notificationUser()
    }

  }

  private fun fetchData() {
    lateinit var jsonString: String
    lateinit var jsonObject: JSONObject
    thread {
      if (isInternetAccess(applicationContext)) {
        val connection = URL(JSON_URL).openConnection() as HttpURLConnection
        jsonString = connection.inputStream.bufferedReader().use { it.readText() }
        jsonObject = JSONObject(jsonString)
        currencyRateMap = getCurrencyMap(jsonObject.getJSONObject("Valute"))
        charCodeArray = ArrayList(currencyRateMap.keys)
        updateArrayAdapter()
        mainHandler.post {
          val old = jsonObject.getString("Date")
          val date =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.ZZ", Locale.getDefault()).parse(old) as Date
          tvData.text = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date)
          progressBar.visibility = View.INVISIBLE
        }
      } else {

        Log.d(tag, "no internet")
        dataBaseHandler = DataBaseHandler(applicationContext, null)
        mainHandler.post {
          tvData.text = getString(R.string.from_db_string)
          progressBar.visibility = View.INVISIBLE
        }
        btnUpdate.visibility = View.VISIBLE
        btnSave.visibility = View.INVISIBLE
        currencyRateMap = dataBaseHandler.getAllCurrencyRates()
        charCodeArray = ArrayList(currencyRateMap.keys)
        updateArrayAdapter()

      }
    }

    Log.d(tag, "End fetch data")
  }

  private fun notificationUser() {
    val input = inSpinner.selectedItem.toString()
    val output = outSpinner.selectedItem.toString()
    val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    val channel =
      NotificationChannel("currency-01", "My notifications", NotificationManager.IMPORTANCE_HIGH)

    manager.createNotificationChannel(channel)
    val builder = NotificationCompat.Builder(this, channel.id).setAutoCancel(true)
      .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("Currency rate")
      .setContentText("${currencyRateMap[input]?.name} : ${currencyRateMap[input]?.value}")
      .setContentInfo("Information")
    manager.notify(1, builder.build())

    val builderSec = NotificationCompat.Builder(this, channel.id).setAutoCancel(true)
      .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("Currency rate")
      .setContentText("${currencyRateMap[output]?.name} : ${currencyRateMap[output]?.value}")
      .setContentInfo("Information")
    manager.notify(2, builderSec.build())
  }

  private fun calculateCurrencyRate(s: String?) {
    val currencyRate: Double
    if (inputSpinnerPosition != outputSpinnerPosition) {
      val charCodeInput = charCodeArray[inputSpinnerPosition]
      val nominal = currencyRateMap[charCodeInput]?.nominal
      val value = currencyRateMap[charCodeInput]?.value

      val charCodeOutput = charCodeArray[outputSpinnerPosition]
      val nominalOut = currencyRateMap[charCodeOutput]?.nominal
      val valueOut = currencyRateMap[charCodeOutput]?.value

      val num = s?.toDoubleOrNull() ?: 0.0
      val df = DecimalFormat("#.####", DecimalFormatSymbols(Locale.ENGLISH))
      currencyRate = num.times(((value!! / nominal!!) / (valueOut!! / nominalOut!!)))
      tvNumber.text = (df.format(currencyRate).toDouble().toString())
    } else {
      tvNumber.text = evNumber.text
    }
  }

  private fun updateArrayAdapter() {
    mainHandler.post {
      arrayAdapter =
        ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item, charCodeArray)
      arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
      inSpinner.adapter = arrayAdapter
      outSpinner.adapter = arrayAdapter
      if (inSpinner.selectedItem.toString().isNotEmpty())
        notificationUser()
    }
  }
}

fun getCurrencyMap(json: JSONObject): MutableMap<String, CurrencyRate> {
  val map = LinkedHashMap<String, CurrencyRate>()
  for (item in json.keys()) {
    val element = json.getJSONObject(item)
    val currencyId = element.getString("ID")
    val numCode = element.getString("NumCode")
    val charCode = element.getString("CharCode")
    val nominal = element.getInt("Nominal")
    val name = element.getString("Name")
    val curr = element.getDouble("Value")
    val prev = element.getDouble("Previous")

    map[item] =
      CurrencyRate(0, currencyId, numCode, charCode, nominal, name, curr, prev)
  }
  return map
}

fun isInternetAccess(context: Context): Boolean {
  val connectivityManager =
    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

  val network = connectivityManager.activeNetwork
  if (network != null) {
    val activeNetwork = connectivityManager.getNetworkCapabilities(network)
    if (activeNetwork != null) {
      if (activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
        return true
      if (activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
        return true
    }
  }
  return false
}

