package com.example.currencyratekt

import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import java.util.concurrent.TimeUnit

class CurrencyRateViewModel(application: Application) : ViewModel() {

  private val workManager = WorkManager.getInstance(application)

  internal fun updateDb() {

    val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
      .setRequiresBatteryNotLow(true)
      .setRequiresStorageNotLow(true)
      .build()

    val workRequest : PeriodicWorkRequest =
      PeriodicWorkRequestBuilder<UpdateDbWorker>(15, TimeUnit.MINUTES)
        .setConstraints(constraints)
        .build()

    workManager.enqueueUniquePeriodicWork("update database", ExistingPeriodicWorkPolicy.KEEP, workRequest)
    Log.d("MyTag", "Workmanager start workrequest")
  }
}
class CurrencyViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    return if (modelClass.isAssignableFrom(CurrencyRateViewModel::class.java)) {
      CurrencyRateViewModel(application) as T
    } else {
      throw IllegalArgumentException("Unknown ViewModel class")
    }
  }
}